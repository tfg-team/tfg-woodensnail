#include "Player.h"


Player::Player() {
	hp = 100;
	damage = 10;
	position.x = 6 * TILE_WIDTH;
	position.y = 15 * TILE_HEIGHT;
	speed.x = 0;
	speed.y = 0;
	isFloating = true;
	isDestroyed = false;
	framesDead = 0;

	collider.h = 72;
	collider.w = 56;

}

void Player::handleEvent(SDL_Event & e) {

	// If a key is pressed
	if (e.type == SDL_KEYDOWN || e.type == SDL_JOYBUTTONDOWN) {
		if (e.key.keysym.sym == MOVE_KEYS::JUMP || e.jbutton.button == SDL_CONTROLLER_BUTTON_A) {
			if (!isFloating) {
				speed.y = -charVelY;
				isFloating = true;
			}
		}
		else if (e.key.keysym.sym == MOVE_KEYS::RIGHT ){
			speed.x = charVelX;
			isFacingRight = true;
			if (!Mix_Playing(1))
				Mix_PlayChannel(1, steps, -1);
		}
		else if (e.key.keysym.sym == MOVE_KEYS::LEFT) {
			speed.x = -charVelX;
			isFacingRight = false;
			if (!Mix_Playing(1))
				Mix_PlayChannel(1, steps, -1);
		}
	}
	else if (e.type == SDL_JOYHATMOTION) {
		 if (e.jhat.value == SDL_HAT_RIGHT) {
			 std::cout << "derecha" << std::endl;
			speed.x = charVelX;
			isFacingRight = true;
			if (!Mix_Playing(1))
				Mix_PlayChannel(1, steps, -1);
		}
		else if (e.jhat.value == SDL_HAT_LEFT) {
			std::cout << "izq" << std::endl;
			speed.x = -charVelX;
			isFacingRight = false;
			if (!Mix_Playing(1))
				Mix_PlayChannel(1, steps, -1);
		}
		else if (e.jhat.value == SDL_HAT_CENTERED) {
			speed.x = 0;
			Mix_HaltChannel(1);
		}
	}
	else if (e.type == SDL_KEYUP) {
		if (e.key.keysym.sym == MOVE_KEYS::RIGHT) {
			speed.x = 0;
			Mix_HaltChannel(1);
		}
		else if (e.key.keysym.sym == MOVE_KEYS::LEFT) {
			speed.x = 0;
			Mix_HaltChannel(1);
		}
	}
}
	


void Player::move() {
	
	Position tilepos;
	Tile tile;
	SDL_Rect t;

	// Check if player falls in a hole
	if (position.y + collider.h >= SCREEN_HEIGHT - (TILE_HEIGHT - TILE_HEIGHT / 3))
		die();

	// X
 	if(speed.x != 0){
		if (isFacingRight) {
			// Check bottom 
			tilepos = { position.x + collider.w  , position.y + collider.h - 10};
			tile = tiles->getTile(tilepos);
			t = tile.getBox();
			if (position.x + collider.w >= t.x  && tile.getType() != -1 && tile.getType() < 12) {
				position.x =  t.x - collider.w;
				speed.x = 0;
			}
			else
				position.x += speed.x;
			// Check head
			tilepos = { position.x + collider.w  , position.y };
			tile = tiles->getTile(tilepos);
			t = tile.getBox();
			if (position.x + collider.w >= t.x  && tile.getType() != -1 && tile.getType() < 12) {
				position.x = t.x - collider.w;
				speed.x = 0;
			}
			else
				position.x += speed.x;
		}
		else {
			// Check bottom 
			tilepos = { position.x , position.y + collider.h - 10};
			tile = tiles->getTile(tilepos);
			t = tile.getBox();
			if(position.x  <= t.x + t.w && tile.getType() != -1 && tile.getType() < 12){
				position.x = t.x + t.w;
				speed.x = 0;
			}
			else
				position.x += speed.x;

			// Check head
			tilepos = { position.x , position.y };
			tile = tiles->getTile(tilepos);
			t = tile.getBox();
			if (position.x <= t.x + t.w && tile.getType() != -1 && tile.getType() < 12){
				position.x = t.x + t.w;
				speed.x = 0;
			}
			else
				position.x += speed.x;
		}

	}
	// Y
	if (speed.y < 0) { 
 		tilepos = { position.x + collider.w / 2 , position.y };
		tile = tiles->getTile(tilepos);
		t = tile.getBox();
		isFloating = true;
		//if (position.y  <= t.y -speed.y && tile.getType() != -1){	
		if (position.y <= t.y + t.h && tile.getType() != -1 && tile.getType() < 12) {
			position.y = t.y + t.h;
			speed.y = 0;
		}
		else
			position.y += speed.y;
		
	}
	else if(speed.y > 0){
		
		tilepos = { position.x + collider.w / 2, position.y + collider.h + speed.y };
		//saveSpeed = speed.y;
		tile = tiles->getTile(tilepos);
		t = tile.getBox();
		if (position.y + collider.h + speed.y >= t.y && tile.getType() != -1 && tile.getType() < 12) {
			speed.y = 0;
			position.y = t.y - collider.h;
			isFloating = false;
		}
		else {
			position.y += speed.y;
		}
	}		
	// Gravity
	if(speed.y < TILE_HEIGHT)
		speed.y += 3;

	// Restraints
	if (position.x < 0)
		position.x = 0;
}

int Player::getMotionStatus(){
	int status = 0;
	if (isFloating) status = MOV_ON_AIR;
	else if (speed.x == 0) status = MOV_IDLE;
	else if (isFacingRight) status = MOV_RIGHT;
	else if (!isFacingRight) status = MOV_RIGHT;
	else if (isDestroyed)	status = MOV_DEAD;
	//else status = MOV_SHOOT;
	
	
	return status;
}

void Player::setColliderDimension(int x, int y, int w, int h) {
	collider.x = x;
	collider.y = y;
	/*collider.w = w;
	collider.h = h;*/
	collider.h = 56;
	collider.w = 32;
}


void Player::render(SDL_Rect& camera, Texture heroTexture, SDL_Renderer* rnd, SDL_Rect* clip, SDL_RendererFlip flip) {
	heroTexture.render(position.x - camera.x, position.y, rnd, clip, flip);	
}

void Player::setCamera(SDL_Rect& camera) {
	// Camera: 10tiles * tile_w = 320 | 
	//if (position.x < 320 && position.x > 0)
	//if (position.x < 0)
	//	camera.x -= 4; // player speed.x
	//else if (position.x > SCREEN_WIDTH - 320 && position.x < 12800)
	//	camera.x += 4; // player speed.x
	//else
		camera.x = (position.x + collider.w / 2) - SCREEN_WIDTH / 2;
	/** **/


}

bool Player::setDamage(float damage){
	hp -= damage;
	if (hp <= 0)
		isDestroyed = true;

	return false;
}

bool Player::getIsFacingRight(){
	return isFacingRight;
}

float Player::getHealth() {
	return hp;
}

Position Player::getPosition(){
	return position;
}

void Player::die(){
	isDestroyed = true;
}

void Player::setTiles(Tilemap *tilemap){
	this->tiles = tilemap;
}

void Player::loadSFX(){
	//Load SFX
	Mix_AllocateChannels(16);
	steps = Mix_LoadWAV("assets/audio/steps2.wav");
	if (steps == NULL) std::cout << "[SFX] Can't load steps sfx" << std::endl;
	Mix_VolumeChunk(steps, 30);
}

SDL_Rect Player::getCollider() {
	return collider;
}

bool Player::getIsDestroyed() {
	return isDestroyed;
}

void Player::setFramesDead(int f){
	framesDead = f;
}

int Player::getFramesDead(){
	return framesDead;
}

float Player::getDamage(){
	return damage;
}
