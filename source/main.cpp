#define SDL_MAIN_HANDLED
#include <System.h>
#include <windows.h>
System sys;

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow){

		// Main game loop
		if (sys.init()) {
			if (sys.loadMedia()) {
				sys.startMenu();
				sys.update();
			}
		}
		sys.quit();
        return 0;
}
