#include "Boss.h"
Boss::Boss() {

}

Boss::Boss(Position pos) {
	position.x = pos.x;
	position.y = pos.y;
	collider.x = pos.x;
	collider.y = pos.y;
	collider.w = 480;
	collider.h = 342;
	isDestroyed = false;
	isFacingRight = false;
	hp = 1500.0;
	damage = 25.0;
}

float Boss::getDamage()
{
	return damage;
}

bool Boss::setDamage(float damage)
{
	hp -= damage;
	if (hp <= 0)
		isDestroyed = true;

	return false;
}
