#pragma once
//Using SDL, SDL_image, standard IO, strings, and file streams
#ifndef TILEMAP_H
#define TILEMAP_H


#include <SDL.h>
#include <SDL_image.h>

#include <stdio.h>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>

#include <rapidxml.hpp>

#include "Texture.h"
#include "Tile.h"

class Tilemap {

public:
	//Tilemap();
	// Initializes the class (width and height in pixels)
	Tilemap(int width, int height);

	// Copy constructor
	//Tilemap(const Tilemap &t);

	// Load tiles from a file and clip it
	bool loadTileset(std::string tileSetIMG, SDL_Renderer* rnd);

	// Loads the tilemap from a file(csv)
	bool loadTilemap(std::string mapFile);

	// Renders the map on the screen
	void render(SDL_Rect& camera, SDL_Renderer* rnd);

	// gettile
	bool getTileType(int x, int y);

	// Returns tile array
	Tile** getTiles();

	// Returns the tile on the pos
	Tile getTile(Position pos);

	// Returns the height of the tilemap
	int getHeight();

	// Returns the width of the tilemap
	int getWidth();

	const int MAX_TILES = 300; //max number of tiles in a spreadsheet

private:
	Texture tilemapTexture;
	SDL_Rect* tileClips;
	Tile **tiles;
	int map[1000][1000];	// Has the tile type
	int width, height; //width and height in tiles
	int *ptr;
};
#endif // !TILEMAP_H