#pragma once
#ifndef ENEMY_H
#define ENEMY_H


#include "Player.h"


class Enemy: public Player{


public:

	Enemy(Position pos);

	void move();

	void setIsFacingRight(bool b);

	int getMotionStatus();

	bool getWithinHero();
	void setWithinHero(bool b);

    float speed = 2.0;

	float getDamage();

	bool setDamage(float damage);

private: 
	float hp;
    float damage;
	bool withinHero;
};

#endif // !ENEMY_H