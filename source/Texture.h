#ifndef TEXTURE_H
#define TEXTURE_H
#ifdef _WIN32
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#else
#include <SDL2/SDL.h>
#endif
#include <iostream>
#include <string>


class Texture {
public:
	
	// Initializes the class
	Texture();

	// Deallocates memory
	//~Texture();

	// Loads the image from file
	bool loadImage(std::string path, SDL_Renderer* rnd);

	// Loads rendered text
	bool loadRenderedText(std::string textureText, SDL_Color color, TTF_Font* font, SDL_Renderer* renderer);

	// Renders the texture on a given point
	void render(int x, int y, SDL_Renderer* rnd, SDL_Rect* clip = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);

	// Sets the texture's alpha
	void setAlpha(Uint8 alpha);

	// Set blending mode
	void setBlendMode(SDL_BlendMode blending);

	// Frees the actual texture
	void free();

	// Gets image dimensions
	int getHeight();
	int getWidth();

private:

	// The actual texture
	SDL_Texture* texture;

	// Image dimensions
	int height, width;

};
#endif
