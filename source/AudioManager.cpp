#include "AudioManager.h"

AudioManager::AudioManager() {}

void AudioManager::Init() {
	// Initialize SDL_Mixer
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
		std::cout << "[AUDIO MANAGER] could not initialize - SDL_mixer Error: " << Mix_GetError() << std::endl;
	else std::cout << "[AUDIO MANAGER] initialized correctly!" << std::endl;
}

void AudioManager::Quit() {

	//Free the music
	Mix_FreeMusic(background);
	background = NULL;

	// Stops SDL_Mixer
	Mix_Quit();
}

bool AudioManager::loadBackgroundMusic(char* path) {
	background = Mix_LoadMUS(path);
	if (background == NULL)
		return false;
	return true;
}

void AudioManager::playBackgroundSong() {
	if (!Mix_PlayingMusic())
		//Play the music
		Mix_PlayMusic(background, -1);
	//If music is being played
	else
	{
		//If the music is paused
		if (Mix_PausedMusic())
			//Resume the music
			Mix_ResumeMusic();
		//If the music is playing
		else
			//Pause the music
			Mix_PauseMusic();
	}
}

void AudioManager::setMusicVolume(int vol) {
	if (vol >= 0)
		Mix_VolumeMusic(vol);
}

void AudioManager::playSFX(Mix_Chunk * sfx, int channel){
		//Play the sfx
		Mix_PlayChannel(channel,sfx,0);
}




