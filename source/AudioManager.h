#pragma once
#include <SDL.h>
#include <SDL_mixer.h>
#include <iostream>
#include <string>

class AudioManager
{
public:
	AudioManager();
	void Init();
	void Quit();
	bool loadBackgroundMusic(char* path);
	void playBackgroundSong();
	void setMusicVolume(int vol);
	static void playSFX(Mix_Chunk* sfx, int channel);
private:
	//Background Music
	Mix_Music *background = NULL;
};
