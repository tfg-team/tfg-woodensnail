
#include "Texture.h"

Texture::Texture(){

	texture = NULL;
	height = 0;
	width = 0;
}

//Texture::~Texture(){
//	// Frees the texture
//	free();
//}


bool Texture::loadImage(std::string path, SDL_Renderer* rnd){

	// Deallocates preexisting texture if so
	free();

	SDL_Texture* newTexture = NULL;

	// Loading the image
	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	if (!loadedSurface)
		std::cout << "[ERROR] Surface " << path << " could not be loaded." << std::endl;
	else {
		
		newTexture = SDL_CreateTextureFromSurface(rnd, loadedSurface);
		if(!newTexture)
			std::cout << "[ERROR] Surface " << path << " could not be converted to texture." << std::endl;
		else {
			// Get image dimensions
			height = loadedSurface->h;
			width = loadedSurface->w;
		}

		// Deallocate the temporal surface
		SDL_FreeSurface(loadedSurface);
	}

	texture = newTexture;
	return texture != NULL;
}

bool Texture::loadRenderedText(std::string textureText, SDL_Color color, TTF_Font* font, SDL_Renderer* renderer) {
	free();
	
	
	SDL_Surface* text = TTF_RenderText_Solid(font, textureText.c_str(), color);
	if (!text) {
		std::cout << "Failed to render text" << std::endl;
		return false;
	}
	else {
		texture = SDL_CreateTextureFromSurface(renderer, text);
		if (!texture) {
			std::cout << "Texture could not be created from surface text" << std::endl;
			return false;
		}
		else {
			height = text->h;
			width = text->w;
		}
			
	}
	SDL_FreeSurface(text);

	
	return true;
}

void Texture::render(int x, int y,  SDL_Renderer* rnd, SDL_Rect* clip, SDL_RendererFlip flip){

	// Specifies the coordinates for SDL_RenderCopy
	SDL_Rect renderQuad = { x, y, width, height };

	if (clip != NULL) {
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	// Render to screen
	SDL_RenderCopyEx(rnd, texture, clip, &renderQuad, 0, nullptr, flip);
}

void Texture::setAlpha(Uint8 alpha){
	SDL_SetTextureAlphaMod(texture, alpha);
}

void Texture::setBlendMode(SDL_BlendMode blending){
	SDL_SetTextureBlendMode(texture, blending);
}

void Texture::free(){
	// Deallocate texture if it exists
	if (texture != NULL) {
		SDL_DestroyTexture(texture);
		texture = NULL;
		height = 0;
		width = 0;
	}
}

int Texture::getHeight(){return height;}

int Texture::getWidth() { return width; }
