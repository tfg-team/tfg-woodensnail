#include "Tilemap.h"

Tilemap::Tilemap(int width, int height) {
	this->width = width/TILE_WIDTH;
	this->height = height/TILE_HEIGHT;
	tiles = new Tile *[width];
	for (int i = 0; i < height; i++)
		tiles[i] = new Tile[height];
}

// Loads the tile spreadsheet (tilesetFile) and clips the image
bool Tilemap::loadTileset(std::string tileSetIMG, SDL_Renderer* rnd) {
	std::cout << "tile path: " << tileSetIMG << std::endl;
	SDL_Rect temp = { 0,0,0,0 };
	int w, h, tileCounter = 0;
	tileClips = new SDL_Rect[MAX_TILES];
	if (!tilemapTexture.loadImage(tileSetIMG, rnd)) {
		std::cout << "[TILEMAP] Tileset could not be loaded" << std::endl;
		return false;
	}
	else {
		w = tilemapTexture.getWidth() / TILE_WIDTH;
		h = tilemapTexture.getHeight() / TILE_HEIGHT;
		for (int j = 0; j < h; j++)
			for (int i = 0; i < w; i++) {
				temp.x = TILE_WIDTH*i;
				temp.y = TILE_HEIGHT*j;
				temp.w = TILE_WIDTH;
				temp.h = TILE_HEIGHT;
				tileClips[tileCounter++] = temp;
			}
	}

	return true;

}
// Loading tile type of every tile of the map
bool Tilemap::loadTilemap(std::string mapFile) {
	std::cout << "Map path: " << mapFile << std::endl;
	std::ifstream file(mapFile);
	int x, y, xoff = TILE_WIDTH, yoff = TILE_HEIGHT, type;
	//Load map from file
	for (int row = 0; row < height; row++) {
		std::string line;
		std::getline(file, line);
		if (!file.good()) {
			std::cout << "[TILEMAP] Map tiles could not be loaded" << std::endl;
			return false;
		}
		std::stringstream iss(line);
		for (int col = 0; col < width; col++) {
			std::string value;
			std::getline(iss, value, ',');
			if (!iss.good()) break;
			std::stringstream convertor(value);
			convertor >> map[row][col];
		}
	}
	//Create tiles of the map
	y = 0;
	for (int i = 0; i < height; i++) {
		x = 0;
		for (int j = 0; j < width; j++) {
			type = map[i][j];
			tiles[i][j] = Tile(x, y, type);
			x += xoff;
		}
		y += yoff;
	}
	return true;
}


void Tilemap::render(SDL_Rect& camera, SDL_Renderer* rnd) {
	for (int i = 0; i < height; i++)
		for (int j = 0; j < width; j++)
			tiles[i][j].render(camera, tilemapTexture, rnd, tileClips);

}

bool Tilemap::getTileType(int x, int y)
{
	if (x > SCREEN_WIDTH / TILE_WIDTH) x = SCREEN_WIDTH / TILE_WIDTH;
	else if (x < 0) x = 0;
	if (y > SCREEN_HEIGHT / TILE_HEIGHT) y = SCREEN_HEIGHT / TILE_HEIGHT - 1;
	else if (y < 0) y = 0;

	if (map[y][x] != -1 && map[y][x] < 12)
		return true;
	else
		return false;
}

Tile** Tilemap::getTiles() {
	return tiles;
}


Tile Tilemap::getTile(Position pos)
{
	return tiles[pos.y / 32][pos.x / 32];
}

int Tilemap::getHeight(){
	return height;
}

int Tilemap::getWidth(){
	return width;
}






