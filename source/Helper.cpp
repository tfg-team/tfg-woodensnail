#include "Helper.h"

namespace Helper {
	//Returns true if the 2 objects are colliding
	bool checkCollision(SDL_Rect a, SDL_Rect b) {

		// Sides of colliders
		int leftA, leftB;
		int rightA, rightB;
		int topA, topB;
		int bottomA, bottomB;

		// Calculate the sides of rect A
		leftA = a.x;
		rightA = a.x + a.w;
		topA = a.y;
		bottomA = a.y + a.h;

		//Calculate the sides of rect B
		leftB = b.x;
		rightB = b.x + b.w;
		topB = b.y;
		bottomB = b.y + b.h;

		if (rightA >= leftB) return true;
		else if (leftA <= rightB) return true;
		else if (bottomA >= topB) return true;
		else if (topA <= bottomB) return true;
		else return false;
	}

	bool checkCollisionBullet(SDL_Rect a, SDL_Rect b, bool goesRight) {

		// Sides of colliders
		int leftA, leftB;
		int rightA, rightB;
		int topA, topB;
		int bottomA, bottomB;

		// Calculate the sides of rect A
		leftA = a.x;
		rightA = a.x + a.w;
		topA = a.y;
		bottomA = a.y + a.h;

		//Calculate the sides of rect B
		leftB = b.x;
		rightB = b.x + b.w;
		topB = b.y;
		bottomB = b.y + b.h;
		
		if (topA >= topB && bottomA <= bottomB) {
			if (goesRight && rightA - leftB >= 0 && rightA - leftB < 20) return true;
			else if (!goesRight && leftA - rightB <= 0 && leftA - rightB > -20) return true;
		}
		
		return false;
	}
}