#include "Tile.h"

Tile::Tile() {}

Tile::Tile(int x, int y, int tileType) {
	//Getting the offsets
	tileBox.x = x;
	tileBox.y = y;
	//Setting width and height
	tileBox.w = TILE_WIDTH;
	tileBox.h = TILE_HEIGHT;

	//Type of the tile
	type = tileType;
}

int Tile::getType() {
	return type;
}

SDL_Rect Tile::getBox() {
	return tileBox;
}

void Tile::render(SDL_Rect& camera, Texture tileTexture, SDL_Renderer* rnd, SDL_Rect tileClips[]) {
	// debug
	SDL_Rect outlineRect = { tileBox.x - camera.x , tileBox.y, tileBox.w, tileBox.h };
	SDL_SetRenderDrawColor(rnd, 0xFF, 0x00, 0xFF, 0xFF);
	//Check if the tile is on the screen
	if (Helper::checkCollision(camera, tileBox) && type != -1 && type < 12) {
		tileTexture.render(tileBox.x - camera.x, tileBox.y - camera.y, rnd, &tileClips[type]);
		//if(type!=-1) SDL_RenderDrawRect(rnd, &outlineRect);
	}
}