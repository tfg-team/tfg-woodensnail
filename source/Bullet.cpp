#include "Bullet.h"

Bullet::Bullet(SDL_Rect &camera, Position position, bool isFacingRight, bool isPlayer, float dmg, bool bossBullet) {
	
	if (!isFacingRight) speed.x = -xSpeed;
	else				speed.x =  xSpeed;
	speed.y = 0;
	this->isPlayer = isPlayer;
	this->position.x = position.x;
	this->position.y = position.y + 28; // Fixed length, it depends on hero texture
	this->bossBullet = bossBullet;
	damage = dmg;
	collider.x = this->position.x;
	collider.y = this->position.y;

	if (!bossBullet) {
		collider.w = 13;
		collider.h = 7;
	}
	else {
		collider.w = 43;
		collider.h = 24;
	}
	t.start();
}

//Bullet::~Bullet(){
//}

void Bullet::move(){
	position.x += speed.x;
	collider.x = position.x;
	
	if (t.getTicks() >= 500) isDestroyed = true;
}

bool Bullet::getIsDestroyed() {
	return isDestroyed;
}

bool Bullet::getIsPlayer() {
	return isPlayer;
}

Position Bullet::getPos(){
	return position;
}

void Bullet::render(SDL_Rect& camera, Texture texture, SDL_Renderer * rnd, SDL_Rect * clip){
	texture.render(position.x - camera.x, position.y, rnd);
}

SDL_Rect Bullet::getCollider() {
	return collider;
}

int Bullet::getSpeed(){
	return speed.x;
}

void Bullet::setIsDestroyed(bool b) {
	isDestroyed = b;
}