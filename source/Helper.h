#include <SDL.h>

namespace Helper {
	// Check if two objects are colliding
	bool checkCollision(SDL_Rect a, SDL_Rect b);

	// Check if a bullet is colliding with an object, being the bullet the first object.
	bool checkCollisionBullet(SDL_Rect a, SDL_Rect b, bool goesRight);
}