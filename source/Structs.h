#pragma once

// Game dimensions
const int SCREEN_WIDTH = 1366;//800;
const int SCREEN_HEIGHT = 768;//600;

// Level dimensions
const int MAP_WIDTH = 12800;
const int MAP_HEIGHT = 768;

// Tile dimensions
const int TILE_WIDTH = 32;
const int TILE_HEIGHT = 32;

// Level dimensions in tiles
const int MAP_TILE_WIDTH = MAP_WIDTH / TILE_WIDTH;
const int MAP_TILE_HEIGHT = MAP_HEIGHT / TILE_HEIGHT;

// Game frames per second
const int GAME_FPS = 60;
const int TICKS_PER_FRAME = 1000 / GAME_FPS;

// Creo que no hace falta
// Number of sprites for each animation
const int HERO_IDLE_FRAMES = 10;

// Constants for motion info
const int MOV_IDLE = 0;
const int MOV_DEAD = 1;
const int MOV_RIGHT = 2;
const int MOV_ON_AIR = 3;
//const int MOV_SHOOT = 4;


// Struct containing the position in 2D coordinates
struct Position {
	int x;
	int y;
};
// Struct containing the velocity of the object
struct Velocity {
	int x;
	int y;
};

// Enum for the controls
enum MOVE_KEYS {
	JUMP = SDLK_SPACE, //& SDLK_w,
	RIGHT = SDLK_RIGHT,// || SDLK_d,
	LEFT = SDLK_LEFT// || SDLK_a
};

const int ENEMY_SCORE_POINTS = 100;
