#include "Player.h"

#pragma once
class Boss: public Player
{
public:
	Boss();
	Boss(Position pos);

	float getDamage();
	bool setDamage(float damage);
private:
	float hp;
	float damage;

};

