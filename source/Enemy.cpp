#include "Enemy.h"

Enemy::Enemy(Position pos) {

	position.x = pos.x;
	position.y = pos.y;
	collider.x = pos.x;
	collider.y = pos.y;
	collider.h = 72;
	collider.w = 56;
	isDestroyed = false;
	isFacingRight = false;
	withinHero = false;
	hp = 30;
	damage = 15;
}

void Enemy::move() {

}

int Enemy::getMotionStatus(){
	int status = 0;
	if (isDestroyed)	status = MOV_DEAD;
	else				status = MOV_IDLE;

	return status;
}

void Enemy::setIsFacingRight(bool b) {
	isFacingRight = b;
}

bool Enemy::getWithinHero() {
	return withinHero;
}

void Enemy::setWithinHero(bool b) {
	withinHero = b;
}

float Enemy::getDamage()
{
	return damage;
}

bool Enemy::setDamage(float damage)
{
	hp -= damage;
	if (hp <= 0)
		isDestroyed = true;

	return false;
}
