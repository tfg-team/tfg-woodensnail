#ifndef TILE_H 
#define TILE_H

//Using SDL, SDL_image, standard IO, strings, and file streams
#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include <fstream>

#include "Texture.h"
#include "Helper.h"
#include "Structs.h"

class Tile
{
public:
	//Empty constructor
	Tile();
	//Initializes position and type
	Tile(int x, int y, int tileType);

	//Shows the tile
	void render(SDL_Rect& camera, Texture tileTexture, SDL_Renderer* rnd, SDL_Rect tileClips[]);

	//Get the tile type
	int getType();

	//Get the collision box
	SDL_Rect getBox();

private:
	//The attributes of the tile
	SDL_Rect tileBox;

	//The tile type
	int type;
};
#endif 