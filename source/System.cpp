#include "System.h"

System::System()
{
}

bool System::init() {

	// Start SDL subsystem
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_JOYSTICK) < 0) {
		printf("[SDL] SDL could not initialize [ERROR] %s\n", SDL_GetError());
		return false;
	}
	else {
		// Enable texture filtering
		SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1");

		window = SDL_CreateWindow("Wooden Snail",
			SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
			SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);

		if (!window) {
			printf("[SDL] Window could not be created [ERROR]%s\n", SDL_GetError());
			return false;
		}
		else {
			renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
			if (!renderer)
				std::cout << "[INIT] Unable to create renderer, SDL_Error: " << SDL_GetError() << std::endl;
			else {
				// Initialize renderer color
				SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);

				// Initialize PNG loading
				if (!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG))
					std::cout << "[INIT] SDL_image could not start. IMG_GetError: " << IMG_GetError() << std::endl;

				// Initialize SDL_ttf
				if(TTF_Init() == -1) {
					std::cout << "Could not start TTF subsystem" << std::endl;
					return false;
				}
				audioManager.Init();
				// Check for joystick and initialize
				if (SDL_NumJoysticks() < 1)
					std::cout << "No Gamepad detected" << std::endl;
				else {
					gamepad = SDL_JoystickOpen(0);
					if (gamepad == NULL)
						std::cout << "Unable to open gamepad! SDL_ERROR: " << SDL_GetError() << std::endl;
					else std::cout << "GAMEPAD initialized without problems!" << std::endl;
				}
			}
		}
	}

	SDL_Surface* icon = SDL_LoadBMP("assets/icon.bmp");
	SDL_SetWindowIcon(window, icon);
	SDL_FreeSurface(icon);

	std::cout << "System initialized without problems!" << std::endl;

	return true;
}

std::vector<SDL_Rect> System::loadSpritesheet(std::string xmlPath){

	std::vector<SDL_Rect> clips;
	SDL_Rect temp = { 0,0,0,0 };
	std::cout << xmlPath << std::endl;
	std::ifstream xmlFile(xmlPath);
	std::vector<char> buffer((std::istreambuf_iterator<char>(xmlFile)), std::istreambuf_iterator<char>());
	buffer.push_back('\0');

	xmlInfo.parse<0>(&buffer[0]);
	root_node = xmlInfo.first_node("TextureAtlas");
	int i = 0;
	// Iteration over nodes
	for (rapidxml::xml_node<>* spriteNode = root_node->first_node("SubTexture"); spriteNode; spriteNode = spriteNode->next_sibling()) {
		temp.x = atoi(spriteNode->first_attribute("x")->value());
		temp.y = atoi(spriteNode->first_attribute("y")->value());
		temp.w = atoi(spriteNode->first_attribute("width")->value());
		temp.h = atoi(spriteNode->first_attribute("height")->value());
		clips.push_back(temp);

	}

	return clips;
}



bool System::loadMedia() {

	//Load Menu and GUI
	if (!menu.loadImage("assets/start.jpg", renderer)) {
		std::cout << "Could not load menu image" << std::endl;
		return false;
	}
	else if (!scoreTexture.loadImage("assets/gui/score.png", renderer)) {
		std::cout << "Could not load score image" << std::endl;
		return false;
	}
	else if (!healthTexture.loadImage("assets/gui/hp.png", renderer)) {
		std::cout << "Could not load hp image" << std::endl;
		return false;
	}
	else if (!timeTexture.loadImage("assets/gui/timer.png", renderer)) {
		std::cout << "Could not load timer image" << std::endl;
		return false;
	}
	else if (!endGameTexture.loadImage("assets/gameover.png", renderer)) {
		std::cout << "Could not load game over image" << std::endl;
		return false;
	}
	else if (!levelCompleteTexture.loadImage("assets/levelComplete.jpg", renderer)) {
		std::cout << "Could not load levelComplete image" << std::endl;
		return false;
	}
	// Load hero sprite sheet texture
	else if (!heroIdleSpritesheet.loadImage("assets/hero/idle.png", renderer)){
		std::cout << "Could not load hero Spritesheet" << std::endl;
		return false;
	}
	else if (!heroDeathSpritesheet.loadImage("assets/hero/dead.png", renderer)) {
		std::cout << "Could not load hero Spritesheet" << std::endl;
		return false;
	}
	else if (!heroRunSpritesheet.loadImage("assets/hero/run.png", renderer)) {
		std::cout << "Could not load hero Spritesheet" << std::endl;
		return false;
	}
	else if (!heroJumpSpritesheet.loadImage("assets/hero/jump.png", renderer)) {
		std::cout << "Could not load hero Spritesheet" << std::endl;
		return false;
	}
	else if (!enemyIdleSpritesheet.loadImage("assets/enemy/idle.png", renderer)) {
		std::cout << "Could not load enemy Spritesheet" << std::endl;
		return false;
	}
	else if (!enemyDeathSpritesheet.loadImage("assets/enemy/dead.png", renderer)) {
		std::cout << "Could not load enemy Spritesheet" << std::endl;
		return false;
	}
	else if (!enemyJumpSpritesheet.loadImage("assets/enemy/jump.png", renderer)) {
		std::cout << "Could not load enemy Spritesheet" << std::endl;
		return false;
	}
	else if (!bulletTexture.loadImage("assets/bullet.png", renderer)) {
		std::cout << "Could not load bullet texture" << std::endl;
		return false;
	}
	else if (!bossBullet.loadImage("assets/bullet2.png", renderer)) {
		std::cout << "Could not load bullet2 texture" << std::endl;
		return false;
	}
	else if (!bossTexture.loadImage("assets/boss/megatank.png", renderer)) {
		std::cout << "Could not load megatank texture" << std::endl;
		return false;
	}
	else {
		heroAnims.push_back(heroIdleClip = loadSpritesheet("assets/hero/idle.xml"));
		heroAnims.push_back(heroDeathClip = loadSpritesheet("assets/hero/dead.xml"));
		heroAnims.push_back(heroRunClip = loadSpritesheet("assets/hero/run.xml"));
		heroAnims.push_back(heroJumpClip = loadSpritesheet("assets/hero/jump.xml"));

		heroTextures.push_back(heroIdleSpritesheet);
		heroTextures.push_back(heroDeathSpritesheet);
		heroTextures.push_back(heroRunSpritesheet);
		heroTextures.push_back(heroJumpSpritesheet);

		enemyAnims.push_back(enemyIdleClip = loadSpritesheet("assets/enemy/idle.xml"));
		enemyAnims.push_back(enemyDeathClip = loadSpritesheet("assets/enemy/dead.xml"));

		enemyTextures.push_back(enemyIdleSpritesheet);
		enemyTextures.push_back(enemyDeathSpritesheet);


	}

	// Load tilemap
	if (!tileMap.loadTileset("assets/tileset.png", renderer))
		std::cout << "Could not load the tileset." << std::endl;
	if(!tileMap.loadTilemap("assets/level1.csv"))
		std::cout << "Could not load the tilemap" << std::endl;
	// Hero has access to tiles
	hero.setTiles(&tileMap);

	//Load music
	if (!audioManager.loadBackgroundMusic("assets/audio/music.mp3"))
		std::cout << "Could not load background music." << std::endl;
	audioManager.setMusicVolume(generalVolume);
	//Load sfx
	hero.loadSFX();
	shoot = Mix_LoadWAV("assets/audio/shoot2.wav");
	if (shoot == NULL) std::cout << "[SFX] Can't load shoot sfx" << std::endl;
	Mix_VolumeChunk(shoot, 25);

	endGameSound = Mix_LoadWAV("assets/audio/endGame.wav");
	if (endGameSound == NULL) std::cout << "[SFX] Can't load endGame sfx" << std::endl;
	Mix_VolumeChunk(endGameSound, 15);
	//Load Background
	if (!background.loadImage("assets/Background.jpg",renderer)) {
		std::cout << "Could not load background" << std::endl;
		return false;
	}

	// Set blendmode
	endGameTexture.setBlendMode(SDL_BLENDMODE_BLEND);
	scorePoints.setBlendMode(SDL_BLENDMODE_BLEND);
	//Spawn enemies
	spawnEnemies();


	//Load font
	infoFont = TTF_OpenFont("assets/fonts/PressStart2P.ttf", 30);
	infoFont2 = TTF_OpenFont("assets/fonts/PressStart2P.ttf", 30);
	TTF_SetFontOutline(infoFont2, 1);
	if (!infoFont) {
		std::cout << "Failed to load font" << std::endl;
		return false;
	}

	return true;
}

void System::handleMovement() {
	Tile tile;
	//SDL_Rect outlineRect;
	hero.move();
	for (int i = 0; i < (int)enemies.size(); i++) enemies[i]->move();
	for (int i = 0; i < (int)bullets.size(); i++) {
		if (bullets[i]->getSpeed() > 0) {
			tile = tileMap.getTile({ bullets[i]->getPos().x + bullets[i]->getCollider().w , bullets[i]->getCollider().y });

			if (bullets[i]->getPos().x + bullets[i]->getCollider().w >= tile.getBox().x  && tile.getType() != -1 && tile.getType() < 12) {
				bullets[i]->setIsDestroyed(true);
			}
			else bullets[i]->move();
			if (bullets[i]->getIsPlayer()) {
				for (int j = 0; j < (int)enemies.size(); j++) {
					if (Helper::checkCollision(camera,enemies[j]->getCollider()) && Helper::checkCollisionBullet(bullets[i]->getCollider(), enemies[j]->getCollider(), true)) {
						enemies[j]->setDamage(bullets[i]->damage);
						bullets[i]->setIsDestroyed(true);
						break;
					}
				}
				if (Helper::checkCollisionBullet(bullets[i]->getCollider(), boss.getCollider(), true)) {
					std::cout << "Hola" << std::endl;
					boss.setDamage(bullets[i]->damage);
					bullets[i]->setIsDestroyed(true);
				}
			}
			else if (Helper::checkCollisionBullet(bullets[i]->getCollider(), hero.getCollider(), true)) {
					hero.setDamage(bullets[i]->damage);
					bullets[i]->setIsDestroyed(true);
				}
		}
		else {
			tile = tileMap.getTile({ bullets[i]->getCollider().x ,bullets[i]->getCollider().y });
			if (bullets[i]->getCollider().x <= tile.getBox().x + tile.getBox().w && tile.getType() != -1 && tile.getType() < 12) {
				bullets[i]->setIsDestroyed(true);
			}
			else
				bullets[i]->move();
			if (bullets[i]->getIsPlayer()) {
				for (int j = 0; j < (int)enemies.size(); j++) {
					if (Helper::checkCollision(camera, enemies[j]->getCollider()) && Helper::checkCollisionBullet(bullets[i]->getCollider(), enemies[j]->getCollider(), false)) {
						enemies[j]->setDamage(bullets[i]->damage);
						bullets[i]->setIsDestroyed(true);
						break;
					}
				}
				if (Helper::checkCollisionBullet(bullets[i]->getCollider(), boss.getCollider(), false)) {
					boss.setDamage(bullets[i]->damage);
					bullets[i]->setIsDestroyed(true);
				}
			}
			else if (Helper::checkCollisionBullet(bullets[i]->getCollider(), hero.getCollider(), false)) {
					hero.setDamage(bullets[i]->damage);
					bullets[i]->setIsDestroyed(true);
				}
		}
	}
	hero.setCamera(camera);
}

void System::gameObjectControl(){

	for (int i = 0; i < (int)bullets.size(); i++)
		if (bullets[i]->getIsDestroyed()) {
			delete bullets[i];
			bullets.erase(bullets.begin() + i);
		}


	for (int i = 0; i < (int)enemies.size(); i++){
		if (hero.getPosition().y <= enemies[i]->getPosition().y + 2 &&
			hero.getPosition().y >= enemies[i]->getPosition().y - 2) {
			if(enemies[i]->getPosition().x - hero.getPosition().x <= 100){
				enemies[i]->setWithinHero(true);
				enemies[i]->setIsFacingRight(false);

				if (!enemyShootTimer.isStarted())
					enemyShootTimer.start();

				if (enemyShootTimer.getTicks() > 700) {
					spawnBullet(enemies[i]->getPosition(), false, false, enemies[i]->getDamage());
					enemyShootTimer.stop();
				}

			}
			else if(hero.getPosition().x - enemies[i]->getPosition().x <= 100
				&& hero.getPosition().x - enemies[i]->getPosition().x > 0){
				enemies[i]->setWithinHero(true);
				enemies[i]->setIsFacingRight(true);

				if (!enemyShootTimer.isStarted())
					enemyShootTimer.start();

				if (enemyShootTimer.getTicks() > 700) {
 					spawnBullet(enemies[i]->getPosition(), true, false, enemies[i]->getDamage());
					enemyShootTimer.stop();
				}
			}
		}


		if (enemies[i]->getIsDestroyed()) {
			if (enemies[i]->getFramesDead() >= 30 ){
				score += ENEMY_SCORE_POINTS;
				delete enemies[i];
				enemies.erase(enemies.begin() + i);
			}
			else
				enemies[i]->setFramesDead(enemies[i]->getFramesDead() + 1);
		}
	}
	if (boss.getIsDestroyed()) {
		score += ENEMY_SCORE_POINTS * 10;
		levelComplete();
	}
	else {
		//boss-script
		if (!rngTimer.isStarted())
			rngTimer.start();

		if (!bossShootTimer.isStarted())
			bossShootTimer.start();


		if (rngTimer.getTicks() >= 2100) {
			random = std::rand() % 3;
			//std::cout << "NEW RANDOM GENERATED" << std::endl;
			rngTimer.stop();
		}
		if (bossShootTimer.getTicks() >= 700) {
			switch (random) {
			case 0:
				//std::cout << " Canon 0 " << std::endl;
				spawnBullet({ boss.getPosition().x, boss.getPosition().y + 15 }, false, false, boss.getDamage(), true);
				break;
			case 1:
				//std::cout << " Canon 1 " << std::endl;
				spawnBullet({ boss.getPosition().x, boss.getPosition().y + boss.getCollider().h/2 - 20}, false, false, boss.getDamage(), true);
				break;
			case 2:
				//std::cout << " Canon 2 " << std::endl;
				spawnBullet({ boss.getPosition().x, boss.getPosition().y + boss.getCollider().h - 80}, false, false, boss.getDamage(), true);
				break;
			}
			bossShootTimer.stop();
		}
	}
}

void System::renderObjects(){
	// Clear screen
	SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderClear(renderer);
	bgOffset = 0 - (camera.x/10) - 150;
	//std::cout << "[Background] X,Y,W,H" << xBG << " " << camera.y << " " << camera.w << " " << camera.h << std::endl;
	background.render(bgOffset, 0, renderer);
	background.render(bgOffset + background.getWidth(), 0, renderer);
	tileMap.render(camera, renderer);


	// Render hero texture
	if (hero.getMotionStatus() == -1)
		std::cout << "[HERO] GetMotionStatus returned -1" << std::endl;
	else
		status = hero.getMotionStatus();



	// Render hero and enemies clips
	SDL_Rect* currentClip = &heroAnims[status][( frames / 15 )  % heroAnims[status].size()];
	if (!hero.getIsFacingRight())
		flip = SDL_FLIP_HORIZONTAL;
	else
		flip = SDL_FLIP_NONE;
	hero.render(camera, heroTextures[status], renderer, currentClip, flip);
	hero.setColliderDimension(hero.getPosition().x, hero.getPosition().y, currentClip->w, currentClip->h);

	// DEBUG
	//SDL_Rect outlineRect = { hero.getCollider().x - camera.x , hero.getCollider().y, currentClip->w, currentClip->h };
	//SDL_SetRenderDrawColor(renderer, 0x00, 0xFF, 0x00, 0xFF);
	//SDL_RenderDrawRect(renderer, &outlineRect);
	//

	/*if(!enemyFlipTimer.isStarted())
		enemyFlipTimer.start();*/

	for (int i = 0; i < (int)enemies.size(); i++) {
		int currentEnemyStatus = enemies[i]->getMotionStatus();
		SDL_Rect* currentEnemyClip = &enemyAnims[currentEnemyStatus][(frames / 15) % enemyAnims[currentEnemyStatus].size()];
		//if (enemyFlipTimer.getTicks() >= 3000){// && !enemies[i]->getWithinHero()) {
			if (enemies[i]->getIsFacingRight()) {
 				enemyFlip = SDL_FLIP_HORIZONTAL;
			}
			else{
				enemyFlip = SDL_FLIP_NONE;
			}
		//	enemyFlipTimer.stop();
		//}
		enemies[i]->render(camera, enemyTextures[enemies[i]->getMotionStatus()], renderer, currentEnemyClip, enemyFlip);
		enemies[i]->setColliderDimension(enemies[i]->getPosition().x, enemies[i]->getPosition().y, currentClip->w, currentClip->h);
	}
	boss.render(camera, bossTexture, renderer, {},bossFlip);
	for (int i = 0; i < (int)bullets.size(); i++) {
		if (!bullets[i]->bossBullet)
			bullets[i]->render(camera, bulletTexture, renderer);
		else
			bullets[i]->render(camera, bossBullet, renderer);
	}

}

void System::renderGUI()
{
	int m, s, ms,ticks;
	std::ostringstream oss;
	SDL_Color white = { 0xFF, 0xFF, 0xFF, 0xFF };
	SDL_Color black = { 0x00, 0x00, 0x00, 0xFF };
	healthTexture.render(10, 10, renderer);
	scoreTexture.render(10, healthTexture.getHeight() + 20, renderer);
	timeTexture.render(SCREEN_WIDTH - timeTexture.getWidth() - timeLeft.getWidth() - 20, 10, renderer);
	oss << hero.getHealth();
	if (!healthPoints.loadRenderedText(oss.str().c_str(), black, infoFont2, renderer))
		std::cout << "Failed to render text" << std::endl;
	healthPoints.render(30 + healthTexture.getWidth(), 15, renderer);
	if (!healthPoints.loadRenderedText(oss.str().c_str(), white, infoFont, renderer))
		std::cout << "Failed to render text" << std::endl;
	healthPoints.render(30 + healthTexture.getWidth(), 15, renderer);
	oss.str("");
	oss << score;
	if (!scorePoints.loadRenderedText(oss.str().c_str(), black, infoFont2, renderer))
		std::cout << "Failed to render text" << std::endl;
	scorePoints.render(30 + healthTexture.getWidth(), healthTexture.getHeight() + 25, renderer);
	if (!scorePoints.loadRenderedText(oss.str().c_str(), white, infoFont, renderer))
		std::cout << "Failed to render text" << std::endl;
	scorePoints.render(30 + healthTexture.getWidth(), healthTexture.getHeight() + 25, renderer);
	oss.str("");
	now = SDL_GetTicks();
	ticks = now - start;
	m = 2 - (ticks / 60000);
	if (m < 0) m = 0;
	s = 59 - ((ticks % 60000) / 1000);
	if (s < 0) s = 0;
	ms = 1000 - ticks % 1000;
	oss << m << ":" << std::setw(2) << std::setfill('0') << s << ":" << std::setw(3) << std::setfill('0') << ms;
	if (!timeLeft.loadRenderedText(oss.str().c_str(), black, infoFont2, renderer))
		std::cout << "Failed to render text" << std::endl;
	timeLeft.render(SCREEN_WIDTH - timeLeft.getWidth() - 10, 15, renderer);
	if (!timeLeft.loadRenderedText(oss.str().c_str(), white, infoFont, renderer))
		std::cout << "Failed to render text" << std::endl;
	timeLeft.render(SCREEN_WIDTH - timeLeft.getWidth() - 10, 15, renderer);

}

void System::startMenu()
{
	bool pressed = false;
	while (!pressed && running) {
		SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
		SDL_RenderClear(renderer);
		menu.render(0, 0, renderer);
		SDL_RenderPresent(renderer);

		while (SDL_PollEvent(&e) != 0) {
			if (e.type == SDL_QUIT)
				running = false;
			else if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_RETURN || e.type == SDL_JOYBUTTONDOWN && e.jbutton.button == 7)
					pressed = true;
		}
	}

}

void System::update() {

	// DEBUG - Spawn location for enemies
	//Position pos = { 10, 350};
	int xMouse = 0, yMouse = 0;



	//d
	Timer bulletTimer;

	fpsTimer.start();

	start = SDL_GetTicks();
	audioManager.playBackgroundSong();

	while (running) {
		capTimer.start();
		bulletTimer.start();


		while (SDL_PollEvent(&e) != 0) {
			if (e.type == SDL_QUIT)
				running = false;
			else if (e.type == SDL_KEYDOWN || e.type == SDL_JOYBUTTONDOWN) {
				if (e.key.keysym.sym == SDLK_x || e.jbutton.button == SDL_CONTROLLER_BUTTON_X) {
					audioManager.playSFX(shoot, -1);
					spawnBullet(hero.getPosition(), hero.getIsFacingRight(), true, hero.getDamage());
				}
				// Keyboard event -> volume change
				else if (e.key.keysym.sym == SDLK_PAGEUP) {
					if (generalVolume < 128) {
						generalVolume++;
						audioManager.setMusicVolume(generalVolume);
					}
				}
				else if (e.key.keysym.sym == SDLK_PAGEDOWN) {
					if (generalVolume > 0) {
						generalVolume--;
						audioManager.setMusicVolume(generalVolume);
					}
				}
			}
			else if (e.type == SDL_MOUSEMOTION) {
				getMouseCoords(xMouse, yMouse);
			}

			hero.handleEvent(e);
		}

		// FPS Cap
		float avgFPS = frames / (fpsTimer.getTicks() / 1000.f);
		//if (avgFPS > 2000000) avgFPS = 0;

		gameObjectControl();
		renderObjects();
		renderGUI();
		handleMovement();

		// Render infoText
		//getAvgFPS(avgFPS);
		//infoTexture.render(SCREEN_WIDTH - infoTexture.getWidth(), 0, renderer);
		//fpsTexture.render(SCREEN_WIDTH - fpsTexture.getWidth(), fpsTexture.getHeight(), renderer);

		// Update screen
		SDL_RenderPresent(renderer);
		frames++;

		// DEBUG
		//pos.x += 10;
		//if (pos.x >= SCREEN_WIDTH) pos.x = 0;

		int frameTicks = capTimer.getTicks();
		if (frameTicks < TICKS_PER_FRAME)
			SDL_Delay(TICKS_PER_FRAME - frameTicks);

			now = SDL_GetTicks();
			if (now - start >= 180000) {
				std::cout << "[SYSTEM] TIMEOUT - YOU LOSE" << std::endl;
				endGame();
			}
			// Control if game ends
			if (hero.getIsDestroyed() && running) {
				if(hero.getFramesDead() >= 300)
				std::cout << "HERO DIES " << std::endl;

				endGame();
			}
		}
	}

void System::spawnEnemies() {
	Position pos;
	Tile** tiles = tileMap.getTiles();
	for(auto i = 0; i < MAP_TILE_HEIGHT; i++)
		for(auto j = 0; j < MAP_TILE_WIDTH; j++)
			if(tiles[i][j].getType() == 12){
				pos.x = tiles[i][j].getBox().x;
				pos.y = tiles[i][j].getBox().y -26;
				//std::cout << "Spawned enemy at: " << pos.x << ", " << pos.y << std::endl;
				enemies.push_back(new Enemy(pos));
			}
			else if (tiles[i][j].getType() == 13) {
				pos.x = tiles[i][j].getBox().x - bossTexture.getWidth()/2;
				pos.y = tiles[i][j].getBox().y - bossTexture.getHeight() + 32;
				boss = Boss(pos);
			}
}

void System::spawnBullet(Position pos, bool isFacingRight, bool isPlayer, float dmg, bool bossBullet) {
	//std::cout << "Bullet spawned at " << pos.x << ", " << pos.y << std::endl;
	if(bossBullet)
		bullets.push_back(new Bullet(camera, pos, isFacingRight, isPlayer, dmg, bossBullet));
	else
		bullets.push_back(new Bullet(camera, pos, isFacingRight, isPlayer, dmg));
}

// DEBUG: Prints the coordinates into screen
void System::getMouseCoords(int x, int y) {
	SDL_GetMouseState(&x, &y);
	std::ostringstream oss;
	// Render text
	SDL_Color textColor = { 0xFF, 0, 0xFF, 0xFF };
	oss << x;
	std::string text = "Mouse coords-> X: " + oss.str();
	text += " Y: ";
	oss.str("");
	oss << y;
	text += oss.str();
	if (!infoTexture.loadRenderedText(text.c_str(), textColor, infoFont, renderer))
		std::cout << "Failed to render text" << std::endl;

}

void System::getAvgFPS(float avgFPS) {
	std::ostringstream oss;
	// Render text
	SDL_Color textColor = { 0xFF, 0, 0xFF, 0xFF };
	oss << avgFPS;
	std::string text = "FPS: " + oss.str();
	if (!fpsTexture.loadRenderedText(text.c_str(), textColor, infoFont, renderer))
		std::cout << "Failed to render text" << std::endl;
}

void System::endGame() {

	// First we flash the screen
	Timer animTimer;
	animTimer.start();


	SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderClear(renderer);
	SDL_RenderPresent(renderer);

	while (animTimer.getTicks() < 1000) {
		audioManager.playSFX(endGameSound, -1);
	}
	Mix_HaltChannel(1);
	Mix_HaltMusic();

	if (audioManager.loadBackgroundMusic("assets/audio/endGameMusic.mp3"))
		std::cout << "load ok" << std::endl;
	audioManager.setMusicVolume(64);
	audioManager.playBackgroundSong();

	Uint8 opacity = 0;
	int sumNow = 0;
	bool endGame = false;
	std::ostringstream oss;
	oss.str("");
	oss << score;

	while (!endGame) {

		// Read input
		while (SDL_PollEvent(&e) != 0) {
			if (e.type == SDL_QUIT)
				running = false;
			else if (e.type == SDL_KEYDOWN ||e.type == SDL_JOYBUTTONDOWN) {
				if (e.key.keysym.sym == SDLK_x || e.jbutton.button == SDL_CONTROLLER_BUTTON_X) {
					audioManager.playSFX(shoot, -1);
					resetGame();
				}
				else if (e.key.keysym.sym == SDLK_ESCAPE || e.jbutton.button == SDL_CONTROLLER_BUTTON_B) {
					endGame = true;
					running = false;
				}
			}
		}

		//Clear screen
		SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
		SDL_RenderClear(renderer);


		// Fade effect
		endGameTexture.setAlpha(opacity);
		endGameTexture.render(0, 0, renderer, nullptr, SDL_FLIP_NONE);


		// Render text
		SDL_Color textColor = { 0xFF, 0xFF, 0xFF, 0xFF };
		scorePoints.setAlpha(opacity);
		if (!scorePoints.loadRenderedText(oss.str().c_str(), textColor, infoFont, renderer))
			std::cout << "Failed to render text" << std::endl;
		scorePoints.render(625, 297, renderer);
		SDL_RenderPresent(renderer);

		if (opacity != 0xFF) {
			if (sumNow == 200) {
				opacity += 0x01;
				sumNow = 0;
			}
			sumNow++;
		}



		frames++;
		int frameTicks = capTimer.getTicks();
		if (frameTicks < TICKS_PER_FRAME)
			SDL_Delay(TICKS_PER_FRAME - frameTicks);
	}
}


void System::resetGame(){

	hero = Player();

	enemies = std::vector<Enemy*>();

	bullets = std::vector<Bullet*>();

	SDL_Rect camera = { 0,0,1366,768 };

	bgOffset = 0;
	score = 0;


	heroTextures = std::vector<Texture>();
	Texture heroIdleSpritesheet = Texture();
	Texture heroDeathClip = Texture();
	Texture heroRunSpritesheet = Texture();
	Texture heroJumpSpritesheet = Texture();


	enemyAnims = std::vector<std::vector<SDL_Rect>>();
	enemyIdleSpritesheet = Texture();
	enemyDeathSpritesheet = Texture();
	enemyRunSpritesheet = Texture();
	enemyJumpSpritesheet = Texture();

	// Bullet texture
	bulletTexture = Texture();

	//Background texture
	background = Texture();

	// Text texture
	infoTexture = Texture();
	fpsTexture = Texture();

	//Audio manager
	//audioManager = AudioManager();
	audioManager.Quit();
	audioManager.Init();

	//Menu
	menu = Texture();
	endGameTexture = Texture();

	//GUI
	healthTexture = Texture();
	healthPoints = Texture();
	scoreTexture = Texture();
	scorePoints = Texture();
	timeTexture = Texture();
	timeLeft = Texture();

	// Timer
	Timer fpsTimer = Timer();
	Timer capTimer = Timer();


	loadMedia();

	update();
}

void System::levelComplete()
{
	// First we flash the screen
	Timer animTimer;
	animTimer.start();

	Mix_HaltMusic();
	SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderClear(renderer);
	SDL_RenderPresent(renderer);


	Uint8 opacity = 0;
	int sumNow = 0;
	bool endGame = false;
	std::ostringstream oss;
	oss.str("");
	oss << score;

	while (!endGame) {

		// Read input
		while (SDL_PollEvent(&e) != 0) {
			if (e.type == SDL_QUIT)
				running = false;
			else if (e.type == SDL_KEYDOWN ||e.type == SDL_JOYBUTTONDOWN) {
				if (e.key.keysym.sym == SDLK_x || e.jbutton.button == SDL_CONTROLLER_BUTTON_X) {
					audioManager.playSFX(shoot, -1);
					resetGame();
				}
				else if (e.key.keysym.sym == SDLK_ESCAPE || e.jbutton.button == SDL_CONTROLLER_BUTTON_B) {
					endGame = true;
					running = false;
				}
			}
		}

		//Clear screen
		SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
		SDL_RenderClear(renderer);


		// Fade effect
		levelCompleteTexture.setAlpha(opacity);
		levelCompleteTexture.render(0, 0, renderer, nullptr, SDL_FLIP_NONE);


		// Render text
		SDL_Color textColor = { 0xFF, 0xFF, 0xFF, 0xFF };
		scorePoints.setAlpha(opacity);
		if (!scorePoints.loadRenderedText(oss.str().c_str(), textColor, infoFont, renderer))
			std::cout << "Failed to render text" << std::endl;
		scorePoints.render(625, 297, renderer);
		SDL_RenderPresent(renderer);

		if (opacity != 0xFF) {
			if (sumNow == 200) {
				opacity += 0x01;
				sumNow = 0;
			}
			sumNow++;
		}



		frames++;
		int frameTicks = capTimer.getTicks();
		if (frameTicks < TICKS_PER_FRAME)
			SDL_Delay(TICKS_PER_FRAME - frameTicks);
	}
}


void System::quit() {
	std::cout << "Quitting SDL" << std::endl;

	// Free loaded images
	heroIdleSpritesheet.free();

	TTF_CloseFont(infoFont);
	infoFont = NULL;

	// Destroy window
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	renderer = NULL;
	window = NULL;

	// Quit SDL subsystems
	IMG_Quit();
	TTF_Quit();
	audioManager.Quit();
	SDL_Quit();
}
