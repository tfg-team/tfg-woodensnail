#pragma once
#ifndef TIMER_H
#define TIMER_H
#ifdef _WIN32
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#else
#include <SDL2/SDL.h>
#endif

class Timer {

public:
	Timer();

	// Timer actions
	void start();
	void stop();
	void pause();
	void unpause();
	void restart();

	// Info retrieval
	Uint32 getTicks();
	bool isStarted();
	bool isPaused();

private:
	
	// Clock time when timer is started
	Uint32 startTicks;

	// Ticks stored when timer was paused
	Uint32 pausedTicks;

	// Timer status
	bool paused;
	bool started;
};
#endif // !TIMER_H
