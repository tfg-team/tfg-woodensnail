//#include "Character.h"
/*
Character class: base class for our characters in the game
*/
#pragma once
#ifndef PLAYER_H
#define PLAYER_H
#ifdef _WIN32
#include <SDL.h>
#include <SDL_image.h>
#include "SDL_mixer.h"
#else
#include <SDL2/SDL.h>
#endif
#undef main

#include <iostream>

#include "Texture.h"
#include "Structs.h"
#include "Helper.h"
#include "Tilemap.h"
#include "AudioManager.h"


class Player{
public:
	Player();
	// Handle incoming events
	void handleEvent(SDL_Event& e);

	// This method will move the character arround the screen
	void move();

	// This method will alow System to swap over animations when moving
	int getMotionStatus();

	// This will change the collider dimensions between clips
	void Player::setColliderDimension(int x, int y,int w, int h);

	// This method will render the texture on character's position in screen
	void render(SDL_Rect& camera, Texture texture, SDL_Renderer* rnd, SDL_Rect* clip = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);

	// Sets the camera relatively to the player
	void setCamera(SDL_Rect& camera);

	// Take the damage from an object
	bool setDamage(float damage);

	// Returns which direction is facing
	bool getIsFacingRight();

	// Returns actual health
	float getHealth();

	// Returns the current position on the screen
	Position getPosition();

	// Starts the death of the player
	void die();

	// Fixed character velocity
	static const int charVelX = 4;
	static const int charVelY = 30;

	void setTiles(Tilemap *t);
	void loadSFX();

	SDL_Rect getCollider();

	bool getIsDestroyed();

	void setFramesDead(int f);
	int getFramesDead();

	float getDamage();

protected:

	Velocity speed;
	Position position;
	SDL_Rect collider;
	bool isFloating;
	bool isFacingRight;
	bool isDestroyed;
	Tilemap *tiles;
	int framesDead;

	Uint32 startTime;
	Mix_Chunk *steps = NULL;
	Mix_Chunk *step1 = NULL;
	Mix_Chunk *step2 = NULL;

private:
	float hp;
	float damage;

};
#endif // !PLAYER_H