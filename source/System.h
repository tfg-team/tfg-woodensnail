#ifndef SYSTEM_H
#define SYSTEM_H
#ifdef _WIN32
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <rapidxml.hpp>
#else
#include <SDL2/SDL.h>
#endif
#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>//d

#include "Structs.h"
#include "Texture.h"
#include "Player.h"
#include "Enemy.h"
#include "Tilemap.h"
#include "Timer.h"
#include "Bullet.h"
#include "AudioManager.h"
#include "Boss.h"


class System{
public:

	System();
    // Initialize variables and SDL subsystem
    bool init();

	// Load spriteSheet's info
	std::vector<SDL_Rect> loadSpritesheet(std::string xmlPath);

	// Load game media
	bool loadMedia();

	// Updates content such as rendering or calculations
	void update();

	// Spawns enemies
	void spawnEnemies();

	// Spawns bullets
	void spawnBullet(Position position, bool isFacingRight, bool isPlayer, float dmg, bool bossBullet = false);

	// DEBUG
	void getMouseCoords(int x, int y);

	// DEBUG
	void getAvgFPS(float avgFPS);

    // Closes and terminates variables and shuts down the game
    void quit();

	//handles movement...
	void handleMovement();
	//handles destruction of dead objects
	void gameObjectControl();
	//handles rendering of objects
	void renderObjects();

	void renderGUI();

	void startMenu();

	void endGame();

	void resetGame();

	void levelComplete();

    //Global vars
    SDL_Window* window = NULL;
    SDL_Surface* screenSurface = NULL;
	bool running = true;
	SDL_Event e;
	long int frames = 0;

	//The window renderer
	SDL_Renderer* renderer = NULL;

	// Textures
	//Texture heroTexture;

	// Font
	TTF_Font* infoFont = NULL;
	TTF_Font* infoFont2 = NULL;

	// Character instances
	Player hero;
	Boss boss;

	std::vector<Enemy*> enemies;

	// Object instances
	std::vector<Bullet*> bullets;

	// Tilemap
	Tilemap tileMap = {MAP_WIDTH, MAP_HEIGHT};
	SDL_Rect camera = { 0,0,1366,768 };
	int bgOffset = 0;

	// Animation vectors
	// Hero
	std::vector<std::vector<SDL_Rect>> heroAnims;
	std::vector<SDL_Rect> heroIdleClip;
	std::vector<SDL_Rect> heroDeathClip;
	std::vector<SDL_Rect> heroRunClip;
	std::vector<SDL_Rect> heroJumpClip;

	std::vector<Texture> heroTextures;
	Texture heroIdleSpritesheet;
	Texture heroDeathSpritesheet;
	Texture heroRunSpritesheet;
	Texture heroJumpSpritesheet;

	SDL_RendererFlip flip;
	SDL_RendererFlip enemyFlip = SDL_FLIP_HORIZONTAL;
	SDL_RendererFlip bossFlip = SDL_FLIP_HORIZONTAL;
	// Enemy
	std::vector<std::vector<SDL_Rect>> enemyAnims;
	std::vector<SDL_Rect> enemyIdleClip;
	std::vector<SDL_Rect> enemyDeathClip;
	std::vector<SDL_Rect> enemyJumpClip;

	std::vector<Texture> enemyTextures;
	Texture enemyIdleSpritesheet;
	Texture enemyDeathSpritesheet;
	Texture enemyRunSpritesheet;
	Texture enemyJumpSpritesheet;
	//Boss
	Texture bossTexture;
	Texture bossBullet;


	rapidxml::xml_document<> xmlInfo;
	rapidxml::xml_node<>* root_node;

	// Hero status (for animations)
	int status;
	// Enemy status vector
	std::vector<int> enemyStatus;

	// Bullet texture
	Texture bulletTexture;

	//Background texture
	Texture background;

	// Text texture
	Texture infoTexture;
	Texture fpsTexture;

	//Audio manager
	AudioManager audioManager;
	Mix_Chunk *shoot = NULL;
	Mix_Chunk *endGameSound = NULL;

	//Menu
	Texture menu;
	Texture endGameTexture;
	Texture levelCompleteTexture;

	//GUI
	Texture healthTexture;
	Texture healthPoints;
	Texture scoreTexture;
	Texture scorePoints;
	Texture timeTexture;
	Texture timeLeft;

	// Timer
	Timer fpsTimer;
	Timer capTimer;
	Timer enemyFlipTimer;
	Timer enemyShootTimer;

	int score = 0;
	int generalVolume = 64;
	int start, now;

	int prevClip = 99;
	int prevClipCount = 0;

	bool enemyDirection;
	//Boss
	int random = 0;
	Timer rngTimer;
	Timer bossShootTimer;
	//GAMEPAD
	SDL_Joystick* gamepad = NULL;


};
#endif // !SYSTEM_H
