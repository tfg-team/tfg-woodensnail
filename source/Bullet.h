#pragma once

#ifndef BULLET_H
#define BULLET_H
#ifdef _WIN32
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#else
#include <SDL2/SDL.h>
#endif
#include <iostream>
#include "Structs.h"
#include "Texture.h"
#include "Timer.h"
#include "Helper.h"
#include "Enemy.h"
#include "Tile.h"

class Bullet {
public:

	Bullet(SDL_Rect &camera, Position position, bool isFacingRight, bool isPlayer, float dmg, bool bossBullet = false);

	//~Bullet();
	
	// This method will move the character arround the screen
	void move();

	//This method will render the texture on bullet's position in screen
	void render(SDL_Rect& camera, Texture texture, SDL_Renderer* rnd, SDL_Rect* clip = NULL);

	bool getIsDestroyed();

	bool getIsPlayer();

	Position getPos();

	void setIsDestroyed(bool b);

	SDL_Rect getCollider();

	int getSpeed();

	float damage;
	
	bool bossBullet = false;
protected:
	Velocity speed;
	Position position;
	SDL_Rect collider;
	SDL_Rect camera;
	const int xSpeed = 20;
	Timer t;
	bool isDestroyed = false;
	bool isPlayer = false;
};
#endif // !BULLET_H
